const components = { index: () => import("@/pages/Index.vue") };

const routes = [{ path: "/", component: components.index }];

export default routes;
