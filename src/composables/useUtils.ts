export const pullToRefresh = async (
  done: () => void,
  fn: () => Promise<void>
) => {
  const { LoadingBar } = await import("quasar");

  LoadingBar.start();

  setTimeout(async () => {
    try {
      await fn();
      LoadingBar.increment(75);
    } finally {
      setTimeout(() => {
        LoadingBar.stop();
        done();
        location.reload();
      }, 750);
    }
  }, 250);
};
