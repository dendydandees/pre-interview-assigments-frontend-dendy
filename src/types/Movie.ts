export type MovieGenre = "Drama" | "Action" | "Animation" | "Sci-Fi" | "Horror";
export interface Movie {
  id: string;
  title: string;
  director: string;
  summary: string;
  genres: MovieGenre[];
}
