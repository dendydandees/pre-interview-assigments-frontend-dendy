export interface DialogProps<Intent = string, Data = Record<string, any>> {
  isShow: boolean;
  intent: Intent;
  dataForm: Data;
  title: string;
  closeLabel: string;
  submitLabel: string;
}

export interface NotificationProps {
  message: string;
  type: "positive" | "negative";
}
