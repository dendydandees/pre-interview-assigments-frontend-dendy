import { createApp } from "vue";

// import vue router
import router from "./router";

// import pinia store
import { createPinia } from "pinia";

// import quasar
import { LoadingBar, LocalStorage, Quasar, Notify } from "quasar";
import quasarIconSet from "quasar/icon-set/svg-material-icons";

// import icon libraries
import "@quasar/extras/roboto-font-latin-ext/roboto-font-latin-ext.css";
import "@quasar/extras/material-icons/material-icons.css";

// A few examples for animations from Animate.css:
// import @quasar/extras/animate/fadeIn.css
// import @quasar/extras/animate/fadeOut.css
// Import Quasar css
import "quasar/src/css/index.sass";

// Assumes your root component is App.vue
// and placed in same folder as main.js
import App from "./App.vue";

const myApp = createApp(App);

myApp.use(Quasar, {
  // import Quasar plugins and add here
  plugins: { LoadingBar, LocalStorage, Notify },
  iconSet: quasarIconSet,
  // check Installation card on each Quasar component/directive/plugin
  config: {
    // settings for LoadingBar Quasar plugin
    loadingBar: { color: "accent", size: "4px" },

    /* brand: {
      primary: '#e46262',
      ... or all other brand colors
    }, */
    /* notify: {...}, // default set of options for Notify Quasar plugin */
    /* loading: {...}, // default set of options for Loading Quasar plugin */
  },
});

// use the vue router
myApp.use(router);

// use the pinia store
const pinia = createPinia();
myApp.use(pinia);

// Assumes you have a <div id="app"></div> in your index.html
myApp.mount("#app");
