import { defineStore } from "pinia";
import { readonly, ref } from "vue";
import { uid } from "quasar";

// import types
import type { Movie, MovieGenre } from "@/types/Movie";

const useMoviesStore = defineStore("movies", () => {
  /** define state **/
  const movies = ref<Movie[]>([
    {
      id: uid(),
      director: "Ridley Scoot",
      genres: ["Drama"],
      summary:
        "An epic that details the checkered rise and fall of French Emperor Napoleon Bonaparte and his relentless journey to power through the prism of his addictive, volatile relationship with his wife, Josephine.",
      title: "Napoleon",
    },
    {
      id: uid(),
      director: "Christopher Nolan",
      genres: ["Drama"],
      summary:
        "The story of J. Robert Oppenheimer's role in the development of the atomic bomb during World War II.",
      title: "Oppenheimer",
    },
  ]);
  const genres = readonly<MovieGenre[]>([
    "Drama",
    "Action",
    "Animation",
    "Horror",
    "Sci-Fi",
  ]);
  const state = { movies, genres };

  /** define getters **/
  const getMovieById = (id: string) =>
    movies.value.find((movie) => movie.id === id);
  const getMovieByTitle = (title: string) =>
    movies.value.filter((movie) =>
      movie.title?.toLowerCase()?.includes(title?.toLowerCase())
    );
  const getters = { getMovieById, getMovieByTitle };

  /** define actions **/
  const addMovie = async (movieData: Omit<Movie, "id">) => {
    const { uid } = await import("quasar");

    const movie = {
      id: uid(),
      ...movieData,
    } satisfies Movie;

    movies.value.push(movie);
  };
  const updateMovie = (updatedMovie: Movie) => {
    const index = movies.value.findIndex(
      (movie) => movie.id === updatedMovie.id
    );

    if (index === -1) throw new Error("Movie not found, add it first!");

    if (index !== -1) {
      movies.value[index] = updatedMovie;
    }
  };
  const deleteMovie = (movieId: string) => {
    movies.value = movies.value.filter((movie) => movie.id !== movieId);
  };
  const actions = { addMovie, updateMovie, deleteMovie };

  return { ...state, ...getters, ...actions };
});

export default useMoviesStore;
