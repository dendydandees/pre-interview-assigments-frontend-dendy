# Todo Watch Movie App

Welcome to the Todo Watch Movie App, a Vue 3 project with TypeScript!

## Install dependencies:

```shell
bun install
```

## Running Development Server

1. Start the development server:

```shell
bun dev
```

2. The address of the local server will be displayed in the terminal, open your browser and visit, then you should see your Todo Watch Movie App up and running!

## Build The Production

1. Build the project for production:

```shell
bun run build
```

2. This will generate a dist folder with the optimized production build.

## Customize Configurations

1. Configuration files:

- tsconfig.json: TypeScript configuration file;

Feel free to modify the configuration files according to your project requirements.

## Folder Structure

- `/public` - Static assets that do not need to go through the build process;
- `/src` - Contains your application source code;
  - `/assets` - Store static assets like images, fonts, etc;
  - `/components` - Reusable Vue components;
  - `/composables` - Reusable composition functions;
  - `/pages` - Vue components representing different views or pages;
  - `/router` - Vue Router configuration;
  - `/stores` - Pinia store modules;
  - `/types` - TypeScript type definitions;
- `App.vue` - The main Vue component;
- `main.ts` - The entry point of your application;

## Learn More

1. [Vue.js Documentation](https://vuejs.org/)

2. [Vue.js Documentation](https://www.typescriptlang.org/docs/)
